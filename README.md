# Blog


## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Status](#status)


## General info
Mobile application - a blog, created on the basis of instagram, but in a more simplified version.


## Technologies
* Java
* Firebase Realtime Database 12.0.1


## Status
Project is:  _finished_


