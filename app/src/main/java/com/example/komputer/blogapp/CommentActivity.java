package com.example.komputer.blogapp;

import android.os.UserHandle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class CommentActivity extends AppCompatActivity {
    private ImageButton comentbtn;
    private RecyclerView comment_list;
    private CommentsAdapter commentsAdapter;
    private EditText CommentTxt;
    private DatabaseReference firebaseDatabase, fireDate;
    private FirebaseAuth mAuth;
    private String post_key, com_key;
    private String currentUserId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Blogzone");
        fireDate = firebaseDatabase.child(post_key + "/Comments");
        currentUserId = mAuth.getCurrentUser().getUid();
        post_key = getIntent().getStringExtra("PostKey");
        com_key = getIntent().getStringExtra("ComKey");
        CommentTxt = findViewById(R.id.commentInp);
        comentbtn = findViewById(R.id.commentInpBtn);
        comment_list = findViewById(R.id.comments_list);
        comment_list.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);

        firebaseDatabase.child(post_key).child("Comments").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String comment = (String) dataSnapshot.child("message").getValue();
                String user = (String) dataSnapshot.child("userId").getValue();
                comment_list.setAdapter(commentsAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        comentbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final String commentmessage = CommentTxt.getText().toString();
                if (!commentmessage.isEmpty())
                {
                    Map<String, Object> commentsMap = new HashMap<>();
                    commentsMap.put("message", commentmessage);
                    commentsMap.put("userId", currentUserId);
                    //commentsMap.put("time", FieldValue.serverTimestamp());
                    firebaseDatabase.child(post_key + "/Comments").push().setValue(commentsMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task)
                        {
                            if (!task.isSuccessful())
                            {
                                Toast.makeText(CommentActivity.this, "Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                            }
                            else
                            {
                                CommentTxt.setText("");
                            }
                        }
                    });
                }
            }

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter<Comments, CommentsViewHolder> firebaseRecyclerAdapter
                =new FirebaseRecyclerAdapter<Comments, CommentsViewHolder>(
                        Comments.class,
                        R.layout.comment_layout,
                        CommentsViewHolder.class,
                        fireDate
                ) {
            @Override
            protected void populateViewHolder(CommentsViewHolder viewHolder, Comments model, int position)
            {
                viewHolder.setUser_id(model.getUser_id());
                viewHolder.setMessage(model.getMessage());
            }
        };
        comment_list.setAdapter(firebaseRecyclerAdapter);
    }

    public static class CommentsViewHolder extends RecyclerView.ViewHolder
    {
        View mView;
        public CommentsViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
        public void setUser_id(String user_id)
        {
            TextView username = (TextView) mView.findViewById(R.id.user);
            username.setText(user_id);
        }
        public void setMessage(String message)
        {
            TextView comment = (TextView) mView.findViewById(R.id.comment);
            comment.setText(message);
        }
    }
}